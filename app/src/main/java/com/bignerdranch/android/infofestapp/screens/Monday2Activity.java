package com.bignerdranch.android.infofestapp.screens;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.bignerdranch.android.infofestapp.R;

public class Monday2Activity extends AppCompatActivity {

    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monday2);

        toolbar = findViewById(R.id.main_toolbar_include_one);

        setSupportActionBar(toolbar);
    }
}
