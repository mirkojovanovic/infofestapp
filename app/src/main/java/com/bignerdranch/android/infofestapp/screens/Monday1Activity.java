package com.bignerdranch.android.infofestapp.screens;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bignerdranch.android.infofestapp.DataHolder;
import com.bignerdranch.android.infofestapp.OnSwipeTouchListener;
import com.bignerdranch.android.infofestapp.R;

public class Monday1Activity extends AppCompatActivity {

    Toolbar toolbar;
    ImageView contentImage;

    DataHolder holder;
    int mMenuResource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monday1);

        contentImage = findViewById(R.id.content_image);

        holder = new DataHolder(false);

        if(holder.isEnglish()) {
            contentImage.setImageResource(R.drawable.rsz_monday1);
        } else {
            contentImage.setImageResource(R.drawable.rsz_monday1_cg);
        }

        toolbar = findViewById(R.id.main_toolbar_include_one);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);


        OnSwipeTouchListener onSwipeTouchListener = new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeRight() {
                startActivity(new Intent(Monday1Activity.this, Monday2Activity.class));

            }
        };

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (holder.isEnglish()) {
            mMenuResource = R.menu.menu_en;
        } else {
            mMenuResource = R.menu.menu_cg;
        }
        getMenuInflater().inflate(mMenuResource, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.en_toggle:
                holder.setEnglish(false);
                invalidateOptionsMenu();
                contentImage.setImageResource(R.drawable.rsz_monday1_cg);
                return true;
            case R.id.me_toggle:
                holder.setEnglish(true);
                invalidateOptionsMenu();
                contentImage.setImageResource(R.drawable.rsz_monday1);
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }

    }
}
