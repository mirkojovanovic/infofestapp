package com.bignerdranch.android.infofestapp;

public class DataHolder {

    private boolean english;

    public DataHolder(boolean english) {
        this.english = english;
    }

    public boolean isEnglish() {
        return english;
    }

    public void setEnglish(boolean english) {
        this.english = english;
    }
}
